# Kocourier Prime Sans 

“Kocourier Sans Prime” is the family of fonts based on the [Courier Prime](https://quoteunquoteapps.com/courierprime/) Sans font made by QuoteUnquote Apps and based on the [typeface project](https://github.com/KyleAMathews/typefaces) for this readme. 
To build the Kocourier Prime Sans font, we used the fontsquirrel generator.

## Install

`npm install --save kocourier-prime-sans`

## Use

Typefaces assume you’re using webpack to process CSS and files. Each typeface
package includes all necessary font files (woff2, woff, eot, ttf, svg) and
a CSS file with font-face declerations pointing at these files.

You will need to have webpack setup to load css and font files. 

To use, simply require the package in your project’s entry file e.g.

```javascript
// Load Open Sans typeface
require('kocourier-prime-sans')
```

## A few words about the Typefaces project.

The goal of the typeface project is to add all open source fonts to NPM to simplify using great fonts in
our web projects. We’re currently maintaining 845 typeface packages
including all typefaces on Google Fonts.

If your favorite typeface isn’t published yet, [let us know](https://github.com/KyleAMathews/typefaces)
and we’ll add it!
